package net.mv.product.dao;

import java.util.List;

import net.mv.product.domain.Product;
import net.mv.product.domain.SubCategory;

public interface ProductDao {
	public void createOrUpdateProduct(Product product);
	public Product findProductByName(String productName);
	public List<SubCategory> getSubByCategoryName(String categoryName);
	public List<Product> getProductsBySubCategoryName(String SubCategoryName);
}
