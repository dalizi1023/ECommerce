package net.mv.product.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.mv.product.domain.Category;
import net.mv.product.domain.Product;
import net.mv.product.domain.SubCategory;

@Repository
public class ProductDaoImpl implements ProductDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void createOrUpdateProduct(Product product) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(product);
	}
	
	@Override
	public Product findProductByName(String productName) {
		Session session = sessionFactory.getCurrentSession();
		Product product = (Product) session.createQuery("from Product where pName=:pName")
								.setString("pName", productName).uniqueResult();
		return product;
	}

	@Override
	public List<SubCategory> getSubByCategoryName(String categoryName) {
		Session session = sessionFactory.getCurrentSession();
		Category category = (Category) session.createQuery("from Category where cName=:categoryName")
						.setString("categoryName", categoryName).uniqueResult();
		return category.getSubCategories();
	}

	@Override
	public List<Product> getProductsBySubCategoryName(String SubCategoryName) {
		Session session = sessionFactory.getCurrentSession();
		SubCategory subCategory = (SubCategory) session.createQuery("from SubCategory where subName=:subName")
						.setString("subName", SubCategoryName).uniqueResult();
		return subCategory.getProducts();
	}

}	
