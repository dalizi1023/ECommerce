package net.mv.product.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class Product {
	@Id
	@Column(name = "P_ID")
	@SequenceGenerator(name = "productSeq", sequenceName = "PRODUCT_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "productSeq", strategy = GenerationType.SEQUENCE)
	private long pId;
	@Column(name="Product_Name",unique=true)
	private String pName;
	@Column
	private String description;
	@Column
	private long amount;
	@Column
	private long price;
	
	@ManyToOne
	@JoinColumn(name="SUB_ID", nullable=false)
	private SubCategory subCategory;

	
	public Product() {
		super();
	}

	public long getpId() {
		return pId;
	}

	public void setpId(long pId) {
		this.pId = pId;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategory) {
		this.subCategory = subCategory;
	}

	@Override
	public String toString() {
		return "Product [pId=" + pId + ", pName=" + pName + ", description=" + description + ", amount=" + amount
				+ ", price=" + price + "]";
	}
	
}
