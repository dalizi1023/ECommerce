package net.mv.product.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="SubCategory")
public class SubCategory {
	@Id
	@Column(name = "Sub_ID")
	@SequenceGenerator(name = "subSeq", sequenceName = "SUB_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "subSeq", strategy = GenerationType.SEQUENCE)
	private long subId;
	@Column(name="SubCategory")
	private String subName;
	
	@ManyToOne
	@JoinColumn(name="C_ID", nullable=false)
	private Category category;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subCategory")
	private List<Product> products;
	
	public long getSubId() {
		return subId;
	}
	public void setSubId(long subId) {
		this.subId = subId;
	}
	public String getSubName() {
		return subName;
	}
	public void setSubName(String subName) {
		this.subName = subName;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
}
