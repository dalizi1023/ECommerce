package net.mv.product.service;

import java.util.List;

import net.mv.product.domain.Product;
import net.mv.product.domain.SubCategory;

public interface ProductService {
	public void createProduct(Product product);
	public void updatePriceByName(String productName,long newPrice);
	public void updateAmoutByName(String productName,long newAmount);
	public List<Product> findProductsBySubCate(String subName);
	public List<SubCategory> findSubByCateName(String cateName);
}
