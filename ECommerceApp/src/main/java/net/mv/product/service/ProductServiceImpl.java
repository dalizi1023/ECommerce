package net.mv.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import net.mv.product.dao.ProductDaoImpl;
import net.mv.product.domain.Product;
import net.mv.product.domain.SubCategory;

public class ProductServiceImpl implements ProductService {
	
	@Autowired
	ProductDaoImpl productDaoImpl;
	
	@Override
	public void createProduct(Product product) {
		productDaoImpl.createOrUpdateProduct(product);	//Can get unique constrain on p_name.
	}

	@Override
	public void updatePriceByName(String productName, long newPrice) {
		Product product = productDaoImpl.findProductByName(productName);		//Record not found?
		product.setPrice(newPrice);
		productDaoImpl.createOrUpdateProduct(product);
	}

	@Override
	public void updateAmoutByName(String productName, long newAmount) {
		Product product = productDaoImpl.findProductByName(productName);
		product.setAmount(newAmount);
		productDaoImpl.createOrUpdateProduct(product);
	}

	@Override
	public List<Product> findProductsBySubCate(String subName) {
		return productDaoImpl.getProductsBySubCategoryName(subName);
	}

	@Override
	public List<SubCategory> findSubByCateName(String cateName) {
		return productDaoImpl.getSubByCategoryName(cateName);
	}

}
