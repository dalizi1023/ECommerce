package net.mv.user.dao;

import net.mv.user.domain.User;

public interface UserDao {
	public void createOrUpdateUser(User user);
	public User getUserByUsername(String username);
}
