package net.mv.user.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="UserRole")
public class UserRole {
	@Id
	@Column(name = "ROLE_ID")
	@SequenceGenerator(name = "roleSeq", sequenceName = "ROLE_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "roleSeq", strategy = GenerationType.SEQUENCE)
	private long roleId;
	@Column
	private String role;
	public long getRoleId() {
		return roleId;
	}
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "UserRole [roleId=" + roleId + ", role=" + role + "]";
	}
	
}
