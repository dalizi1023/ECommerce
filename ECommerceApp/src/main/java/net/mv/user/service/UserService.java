package net.mv.user.service;

import net.mv.user.domain.User;

public interface UserService {
	public void registerUser(User user);
	public void updateUser(User user);
	public User authenticateUser(User user);
}
