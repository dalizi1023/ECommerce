package net.mv.user.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.mv.user.dao.UserDaoImpl;
import net.mv.user.domain.User;

@Transactional
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserDaoImpl userDaoImpl;
	
	@Override
	public void registerUser(User user) {
		userDaoImpl.createOrUpdateUser(user);		//Can throw unique constrain violation.
	}

	@Override
	public void updateUser(User user) {
		userDaoImpl.createOrUpdateUser(user);		//Can throw unique constrain violation.
	}

	@Override
	public User authenticateUser(User user) {
		User dBUser = userDaoImpl.getUserByUsername(user.getUsername());
		if(dBUser.getPassword() == user.getPassword()){
			return dBUser;
		}else{
			return user;
		}
	}

}
