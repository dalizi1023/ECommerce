package net.mv.util;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UtilControler {

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView viewHomePage(){
		ModelAndView nextPage = new ModelAndView();
		nextPage.setViewName("index");
		return nextPage;
	}
}
