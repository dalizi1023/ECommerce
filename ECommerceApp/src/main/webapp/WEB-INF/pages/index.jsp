<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Online Clothing Store</title><base>
	<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css"/>">
	
</head>
<body>


<!-- ======================================== NAVABR MENUE 1 ======================================== -->
	<div id="top-nav">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-nav-collapse" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <!-- <a class="navbar-brand" href="#">ECommerce</a> -->
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="top-nav-collapse">
		    <!--   <ul class="nav navbar-nav">
		        <li><a href="#">Link <span class="sr-only">(current)</span></a></li>
		        <li><a href="#">Link</a></li>
		      </ul> -->
		      
		    <!--   <form class="navbar-form navbar-left" role="search">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Search">
		        </div>
		        <button type="submit" class="btn btn-default">Submit</button>
		      </form> -->
		      <ul class="nav navbar-nav navbar-right">
		      	<li><a href="#">Help</a></li>
		        <li><a href="#">Contact</a></li>
		        <li><a href="#">My Account</a></li>
		        <li><a href="#">My Orders</a></li>
		        <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
	
	<!--  Company logo -->
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div id="logo">
					<h1>ECommerce</h1>
				</div>
			</div>
		</div>
	</div>
	
	<!-- ======================================== NAVABR MENUE 2 (Categories) ======================================== --><a></a>
	<div id="cat-nav" class="container">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li><a href="#">Men</a></li>
		        <li><a href="#">Women</a></li>
		        <li><a href="#">Sale</a></li>
		      </ul>
		      
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
	<!-- ======================================== SLIDESHOW ======================================== -->
	<div id="slide-container" class="container fill">
		<div id="slideshow" class="carousel slide" data-ride="carousel" data-interval="5000">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#slideshow" data-slide-to="0" class="active"></li>
		    <li data-target="#slideshow" data-slide-to="1"></li>
		    <li data-target="#slideshow" data-slide-to="2"></li>
		  </ol>
		 
		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		      <img src="https://images.unsplash.com/photo-1445205170230-053b83016050?crop=entropy&fit=crop&fm=jpg&h=1000&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=1925" alt="...">
		      <div class="carousel-caption">
		          <h3>Caption Text</h3>
		      </div>
		    </div>
		    <div class="item">
		      <img src="https://images.unsplash.com/photo-1445205170230-053b83016050?crop=entropy&fit=crop&fm=jpg&h=1000&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=1925" alt="...">
		      <div class="carousel-caption">
		          <h3>Caption Text</h3>
		      </div>
		    </div>
		    <div class="item">
		      <img src="https://images.unsplash.com/photo-1445205170230-053b83016050?crop=entropy&fit=crop&fm=jpg&h=1000&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=1925" alt="...">
		      <div class="carousel-caption">
		          <h3>Caption Text</h3>
		      </div>
		    </div>
		  </div>
		 
		  <!-- Controls -->
			  <a class="left carousel-control" href="#slideshow" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left"></span>
			  </a>
			  <a class="right carousel-control" href="#slideshow" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right"></span>
			  </a>
		</div> <!-- Carousel -->
	 </div>
	<h1>Test Message</h1>
	<%-- <jsp:include page="footer.jsp"></jsp:include> --%>
	<!-- ================================== jQuery and bootstrap.min.js ======================== -->
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.12.1.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
</body>
</html>